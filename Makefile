SHELL = /bin/sh
CC = gcc

DESTDIR = 

PREFIX = $(DESTDIR)/usr/local
MANDIR = $(PREFIX)/man/man1
DOCDIR = $(PREFIX)/share/doc

# Image library
USE_GDKPIXBUF2 = 1
#USE_GDKPIXBUF = 1
#USE_IMLIB = 1

# Operating system
OS = -DLINUX
#OS = -DSOLARIS
#OS = -DFREEBSD

INSTALL = /usr/bin/install
RM = rm -f

DEFS = $(OS)

ifdef USE_GDKPIXBUF
IMGLIB_DEF = -DUSE_GDKPIXBUF
CFLAGS = -O3 -Wall `gdk-pixbuf-config --cflags`
LDFLAGS = `gdk-pixbuf-config --libs` -lgdk_pixbuf_xlib
endif

ifdef USE_GDKPIXBUF2
IMGLIB_DEF = -DUSE_GDKPIXBUF2
CFLAGS = -O3 -Wall `pkg-config --cflags gdk-pixbuf-xlib-2.0` -I/usr/X11R6/include
LDFLAGS = `pkg-config --libs gdk-pixbuf-xlib-2.0` -rdynamic -lX11 -lXi -lXext -L/usr/X11R6/lib
endif

ifdef USE_IMLIB
IMGLIB_DEF = -DUSE_IMLIB
CFLAGS = -O3 -Wall `imlib-config --cflags`
LDFLAGS = `imlib-config --libs`
endif

DEFS += $(IMGLIB_DEF)

ifeq ($(OS),-DSOLARIS)
	LDFLAGS += -lposix4 -lz -ltiff -ljpeg
	DEFS += -DUSE_GETOPT
endif

ifeq ($(OS),-DFREEBSD)
	LDFLAGS += -lgnugetopt
endif

PRGS = wmdrawer
SRCS = utils.c config.c graphics.c images.c wmdrawer.c
OBJS = $(SRCS:.c=.o)

all : $(PRGS)

.c.o :
	$(CC) $(CFLAGS) $(DEFS) -c $<

wmdrawer : $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS)
	strip $@

clean :
	$(RM) *.o $(PRGS)

install : wmdrawer
	$(INSTALL) -d $(PREFIX)/bin/
	$(INSTALL) -m 755 wmdrawer $(PREFIX)/bin/
	$(INSTALL) -d $(MANDIR)
	$(INSTALL) -m 644 doc/wmdrawer.1x.gz $(MANDIR)
	$(INSTALL) -d $(DOCDIR)/wmdrawer
	$(INSTALL) -m 644 wmdrawerrc.example AUTHORS ChangeLog COPYING INSTALL README TODO $(DOCDIR)/wmdrawer

uninstall: clean
	$(RM) $(PREFIX)/bin/wmdrawer
	$(RM) $(MANDIR)/wmdrawer.1x.gz
	$(RM) -rf $(DOCDIR)/wmdrawer
