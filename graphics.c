/* wmDrawer (c) 2002-2012 Valéry Febvre <vfebvre@easter-eggs.com>
 *
 * wmDrawer is a dock application (dockapp) which provides a
 * drawer (button bar) to launch applications from.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "types_defs.h"
#include "graphics.h"

/* This function come from aterm 0.4.2 (src/ximage_utils.c)
 *  Copyright (c) 2001 René Scharfe     <l.s.r at web dot de>
 *  Copyright (c) 2001,1999 Sasha Vasko <sashav at sprintmail dot com> */
void ShadeXImage (Display *dpy, XImage* srcImage, ShadingInfo* shading) {
  unsigned int int_rm, int_gm, int_bm ;
  int shade = 0;
  register unsigned long p, x;
  unsigned long y;
  unsigned int rb_mask, g_mask;
  unsigned char r_shift=16, g_shift=8, b_shift;

  Visual* visual = DefaultVisual(dpy, DefaultScreen(dpy));

  if( visual->class != TrueColor || srcImage->format != ZPixmap ) return ;
  if( srcImage->bits_per_pixel <= 8 ) return ;
  if( shading->shading < 0 ) shade = 100-shading->shading ; /*we want to lighten instead of darken */
  else shade = shading->shading ;
  if( shade > 200 ) shade = 200 ;
  int_rm = (shading->tintColor.red>>8)*shade/100;
  int_gm = (shading->tintColor.green>>8)*shade/100;
  int_bm = (shading->tintColor.blue>>8)*shade/100;
  
  g_mask = rb_mask = 0xf8 ;
  b_shift = 3 ;
  switch (srcImage->bits_per_pixel) {
  case 15: r_shift = 7 ; g_shift = 2 ; break ;
  case 16: g_mask = 0xfc ; r_shift = 8 ; g_shift = 3 ; break ;
  case 32:
  case 24: g_mask = rb_mask = 0xff ; r_shift = 16 ; g_shift = 8 ; break ;
  }

  if (srcImage->bits_per_pixel<=16) { /* for better speed we don't want to check for it inside the loop */
    for (y = 0; y < srcImage->height; y++)
      for (x = 0; x < srcImage->width; x++) {
	p = XGetPixel(srcImage, x, y);
	XPutPixel(srcImage, x, y,
		  ((((((p>>r_shift)&rb_mask)*int_rm)/255)&rb_mask)<<r_shift)|
		  ((((((p>>g_shift)&g_mask)*int_gm)/255)&g_mask)<<g_shift)|
		  ((((((p<<b_shift)&rb_mask)*int_bm)/255)&rb_mask)>>b_shift));
      }
  }
  else {
    for (y = 0; y < srcImage->height; y++)
      for (x = 0; x < srcImage->width; x++) {
	p = XGetPixel(srcImage, x, y);
	XPutPixel(srcImage, x, y,
		  ((((((p>>r_shift)&rb_mask)*int_rm)/255)&rb_mask)<<r_shift)|
		  ((((((p>>g_shift)&g_mask)*int_gm)/255)&g_mask)<<g_shift)|
		  ((((p & rb_mask)*int_bm)/255)&rb_mask));
      }
  }
}
