/* wmDrawer (c) 2002-2012 Valéry Febvre <vfebvre@easter-eggs.com>
 *
 * wmDrawer is a dock application (dockapp) which provides a
 * drawer (button bar) to launch applications from.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "utils.h"

#define PACKAGE "wmdrawer"
#define VERSION "cvs"
#define RELEASE_DATE "yyyy-mm-dd"

#define MAX_ICON_SIZE 128
#define MIN_ICON_SIZE 12

#define DBG_LEVEL 0

enum dir {
  topToBottom,
  rightToLeft,
  bottomToTop,
  leftToRight
};

typedef struct ShadingInfo {
  XColor tintColor;
  int shading;
} ShadingInfo;

typedef struct _drawingArea {
  Window win;
  Pixmap pixmap;
  GC     gc;
  XGCValues xgcv;
  XSetWindowAttributes xswa;
} drawingArea;

typedef struct _drawerButton {
  drawingArea da;
  unsigned int isEmpty;
  char *tooltip;
  char *image;
  char *command;
} drawerButton;

typedef struct _dockButton {
  drawingArea da;
  unsigned int isEmpty;
} dockButton;

typedef struct _drawerConfig {
  char *dockIcon;
  char *iconsBg;
  int  iconsExpand;
  int  transparency;
  int  dockW;
  int  dockH;
  int  dockShading;
  int  btnsSize;
  int  direction;
  int  animationSpeed;
  int  cursor;
  int  borderSize;
  int  showOnHover;
  int  hideOnOut;
  int  hideTimeout;
  int  windowedMode;
  int  windowedModeLocked;
  char *instanceName;
  int  highlight;
  char *highlightImg;
  ShadingInfo highlightShading;
  int tooltips;
  char *tooltipsFont;
  time_t lastModif;  
  char **imagesPaths;
  drawerButton **entries;
} drawerConfig;
