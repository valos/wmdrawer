/* wmDrawer (c) 2002-2012 Valéry Febvre <vfebvre@easter-eggs.com>
 *
 * wmDrawer is a dock application (dockapp) which provides a
 * drawer (button bar) to launch applications from.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#define RESIZE_SMALLER (1UL << 0)
#define RESIZE_GREATER (1UL << 1)

int  createPixmapFromFile (char *file, Pixmap *imgPix, Pixmap *imgMask,
			   int *w, int *h, int dimW, int dimH,
			   unsigned long resizeMask);
void createPixmapFromData (const char **data, Pixmap *imgPix, Pixmap *imgMask,
			   int *w, int *h, int dimW, int dimH,
			   unsigned long resizeMask);
