# Description

wmDrawer is a dock application (dockapp) which provides a
drawer (retractable button bar) to launch applications.

- Homepage : http://people.easter-eggs.org/~valos/wmdrawer/
- Source   : https://gitlab.com/valos/wmdrawer/

## Features

- The drawer is retractable and animated.
- Several columns of buttons can be defined in the drawer.
- The config file is automaticaly reloaded if it changes.
- Multiple instances of the program can run at the same time.
- The buttons can be highlighted under the mouse cursor.
- A tooltip can be defined for each button.
- The drawer can be transparent (need SHAPE extension).
- Ability to launch several apps at once from the drawer.
  By holding down the key Shift or by using mouse buttons
  middle or right instead of left, you can keep the drawer
  opened.

## Adjustable behaviors:

- dock's image (logo)
- dock's width and height (for both min=12, max=128)
- dock's shading
- automatic/manual opening and closing of the drawer
- timeout value in miliseconds if automatic closing selected
- drawer's opening direction and animation speed
- mouse cursor type in the drawer
- drawer's pseudo-transparency
- icons' background image (only if transparency isn't activated)
- buttons' size (min=12, max=128)
- buttons' highlight type (0:none, 1:target, 2:shading)
- image used as highlight (target type only)
- color and darkening level for highlight (shading type only)
- drawer's border size
- buttons' tooltips and tooltips' font

## Bugs

If you discover any bug in this software, please fill an [issue](https://gitlab.com/valos/wmdrawer/issues) and describe the problem with
as many details as possible.

## Copyright

wmDrawer is Copyright (C) 2002-2020 by Valéry Febvre, FRANCE

wmDrawer is licensed through the GNU General Public License.
Read the COPYING file for the complete GNU license.

# Usage

Simply launch wmdrawer binary:

```
$ wmdrawer
```

You need to write a config file (.wmdrawerrc) and put it in
your home directory (look at INSTALL and wmdrawerrc.example
files for futher information).

To launch wmDrawer with a specific configuration file (useful
if you want to launch several instances of wmDrawer)

```
$ wmdrawer -c config_file [-n instance_name]
```

To launch wmDrawer in windowed mode (useful for AfterStep,
Fvwm, Sawfish or KDE users)

```
$ wmdrawer -w
```

To display the version of wmDrawer

```
$ wmdrawer -v
```
